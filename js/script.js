

function remmoveArg(arr, parentDom) {
  const elemUl = document.createElement("ul");
  parentDom.append(elemUl);

  arr.forEach((elem) => {
    const elemLi = document.createElement("li");
    elemLi.innerHTML = elem;
    document.querySelector("ul").append(elemLi);
  });
}

remmoveArg(
  ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"],
  document.body
);
